from pyjamas.ui.RootPanel import RootPanel
from pyjamas.ui.HorizontalPanel import HorizontalPanel
from pyjamas.ui.VerticalPanel import VerticalPanel
from pyjamas.ui.Button import Button
from pyjamas.ui.HTML import HTML
from pyjamas import Window
from pyjamas.ui.FlexTable import FlexTable
from pyjamas.ui.SimplePanel import SimplePanel

import pygwt

# ~~~~~~

def hit_character(sender):
    character = sender.character
    index = sender.tbl.characters.index(character)+1
    dam_str = Window.prompt('Damage to %s:' % character.name)    
    if dam_str != None and dam_str != '':
        damage = int(dam_str)
        character.hit(damage)
        sender.tbl.tbl.setHTML(index,2,str(character.hits))

        
class hitbutton(Button):
    def __init__(self,tbl,character):
        Button.__init__(self, 'Hit', hit_character)
        self.tbl = tbl
        self.character = character
        self.setStyleName('button')


# ~~~~~~        
        
def kill_character(sender):
    character = sender.character
    index = sender.tbl.characters.index(character)+1
    if Window.confirm('Kill %s?' % character.name):
        sender.tbl.tbl.removeRow(index)
        sender.tbl.characters.remove(character)

        
class killbutton(Button):
    """ the button what kills you(r character) """
    def __init__(self,tbl,character):
        Button.__init__(self, 'Kill', kill_character)
        self.tbl = tbl
        self.character = character
        self.setStyleName('button')

        
# ~~~~~~
        
class character:
    def __init__(self,name,initiative):
        self.name = name
        self.hits = 0
        self.initiative = initiative

    def hit(self,damage):
        self.hits += damage

        
class pane(SimplePanel):
    def __init__(self):
        SimplePanel.__init__(self)
        self.tbl = FlexTable()
        self.tbl.setBorderWidth(0)
        #self.tbl.setWidth('500px')
        self.tbl.setHTML(0,0,'<b>+</b>')
        self.tbl.setHTML(0,1,'<b>Name</b>')
        self.tbl.setHTML(0,2,'<b>Dmg</b>')
        self.characters = []
        self.add(self.tbl)
        self.initiative = 0
        self.pressed = False

    def add_character(self,character):
        self.characters += [character,]
        self.characters.sort(key = lambda c : -c.initiative)
        index = self.characters.index(character)+1
        self.tbl.insertRow(index)
        self.tbl.setHTML(index,1,character.name)
        self.tbl.setHTML(index,2,str(character.hits))
        self.tbl.setWidget(index,3,hitbutton(self,character))
        self.tbl.setWidget(index,4,killbutton(self,character))

    def advance(self):
        if self.pressed:
            self.tbl.setHTML(self.initiative+1,0,'')
            self.initiative += 1
            if self.initiative >= len(self.characters):
                self.initiative = 0
            self.tbl.setHTML(self.initiative+1,0,'>')
        else:
            self.pressed = True
            self.tbl.setHTML(self.initiative+1,0,'>')

tbl = pane()
control = HorizontalPanel()

def add_character(sender):
    name = Window.prompt('Enter a name for the character.')
    initiative = int( Window.prompt('Initiative for %s:' % name) )
    if name != None and name != '':
        tbl.add_character(character(name,initiative))

def end_turn(sender):
    if len(tbl.characters)>0:
        tbl.advance()

if __name__ == '__main__':

    plus = Button("Add Character", add_character, StyleName='button')
    turn = Button("End Turn", end_turn, StyleName='button')

    control.add(plus)
    control.add(turn)

    RootPanel().add(control)
    RootPanel().add(tbl)
