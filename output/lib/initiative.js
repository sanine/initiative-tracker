/* start module: initiative */
$pyjs['loaded_modules']['initiative'] = function (__mod_name__) {
	if($pyjs['loaded_modules']['initiative']['__was_initialized__']) return $pyjs['loaded_modules']['initiative'];
	var $m = $pyjs['loaded_modules']['initiative'];
	$m['__repr__'] = function() { return '<module: initiative>'; };
	$m['__was_initialized__'] = true;
	if ((__mod_name__ === null) || (typeof __mod_name__ == 'undefined')) __mod_name__ = 'initiative';
	$m['__name__'] = __mod_name__;


	$m['RootPanel'] = $p['___import___']('pyjamas.ui.RootPanel.RootPanel', null, null, false);
	$m['HorizontalPanel'] = $p['___import___']('pyjamas.ui.HorizontalPanel.HorizontalPanel', null, null, false);
	$m['VerticalPanel'] = $p['___import___']('pyjamas.ui.VerticalPanel.VerticalPanel', null, null, false);
	$m['Button'] = $p['___import___']('pyjamas.ui.Button.Button', null, null, false);
	$m['HTML'] = $p['___import___']('pyjamas.ui.HTML.HTML', null, null, false);
	$m['Window'] = $p['___import___']('pyjamas.Window', null, null, false);
	$m['FlexTable'] = $p['___import___']('pyjamas.ui.FlexTable.FlexTable', null, null, false);
	$m['SimplePanel'] = $p['___import___']('pyjamas.ui.SimplePanel.SimplePanel', null, null, false);
	$m['pygwt'] = $p['___import___']('pygwt', null);
	$m['hit_character'] = function(sender) {
		var index,$and1,$and2,character,damage,$add2,$add1,dam_str;
		character = $p['getattr'](sender, 'character');
		index = $p['__op_add']($add1=sender['tbl']['characters']['index'](character),$add2=1);
		dam_str = $m['Window']['prompt']($p['sprintf']('Damage to %s:', $p['getattr'](character, '$$name')));
		if ($p['bool'](($p['bool']($and1=!$p['op_eq'](dam_str, null))?!$p['op_eq'](dam_str, ''):$and1))) {
			damage = $p['float_int'](dam_str);
			character['hit'](damage);
			sender['tbl']['tbl']['setHTML'](index, 2, $p['str']($p['getattr'](character, 'hits')));
		}
		return null;
	};
	$m['hit_character']['__name__'] = 'hit_character';

	$m['hit_character']['__bind_type__'] = 0;
	$m['hit_character']['__args__'] = [null,null,['sender']];
	$m['hitbutton'] = (function(){
		var $cls_definition = new Object();
		var $method;
		$cls_definition['__module__'] = 'initiative';
		$method = $pyjs__bind_method2('__init__', function(tbl, character) {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
				tbl = arguments[1];
				character = arguments[2];
			}

			$m['Button']['__init__'](self, 'Hit', $m['hit_character']);
			self['tbl'] = tbl;
			self['character'] = character;
			self['setStyleName']('button');
			return null;
		}
	, 1, [null,null,['self'],['tbl'],['character']]);
		$cls_definition['__init__'] = $method;
		var $bases = new Array($m['Button']);
		var $data = $p['dict']();
		for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
		return $p['_create_class']('hitbutton', $p['tuple']($bases), $data);
	})();
	$m['kill_character'] = function(sender) {
		var $add3,character,$add4,index;
		character = $p['getattr'](sender, 'character');
		index = $p['__op_add']($add3=sender['tbl']['characters']['index'](character),$add4=1);
		if ($p['bool']($m['Window']['confirm']($p['sprintf']('Kill %s?', $p['getattr'](character, '$$name'))))) {
			sender['tbl']['tbl']['removeRow'](index);
			sender['tbl']['characters']['remove'](character);
		}
		return null;
	};
	$m['kill_character']['__name__'] = 'kill_character';

	$m['kill_character']['__bind_type__'] = 0;
	$m['kill_character']['__args__'] = [null,null,['sender']];
	$m['killbutton'] = (function(){
		var $cls_definition = new Object();
		var $method;
		$cls_definition['__module__'] = 'initiative';
		$method = $pyjs__bind_method2('__init__', function(tbl, character) {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
				tbl = arguments[1];
				character = arguments[2];
			}

			$m['Button']['__init__'](self, 'Kill', $m['kill_character']);
			self['tbl'] = tbl;
			self['character'] = character;
			self['setStyleName']('button');
			return null;
		}
	, 1, [null,null,['self'],['tbl'],['character']]);
		$cls_definition['__init__'] = $method;
		var $bases = new Array($m['Button']);
		var $data = $p['dict']();
		for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
		return $p['_create_class']('killbutton', $p['tuple']($bases), $data);
	})();
	$m['character'] = (function(){
		var $cls_definition = new Object();
		var $method;
		$cls_definition['__module__'] = 'initiative';
		$method = $pyjs__bind_method2('__init__', function(name, initiative) {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
				name = arguments[1];
				initiative = arguments[2];
			}

			self['$$name'] = name;
			self['hits'] = 0;
			self['initiative'] = initiative;
			return null;
		}
	, 1, [null,null,['self'],['name'],['initiative']]);
		$cls_definition['__init__'] = $method;
		$method = $pyjs__bind_method2('hit', function(damage) {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
				damage = arguments[1];
			}
			var $add6,$add5;
			self['hits'] = $p['__op_add']($add5=$p['getattr'](self, 'hits'),$add6=damage);
			return null;
		}
	, 1, [null,null,['self'],['damage']]);
		$cls_definition['hit'] = $method;
		var $bases = new Array(pyjslib['object']);
		var $data = $p['dict']();
		for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
		return $p['_create_class']('character', $p['tuple']($bases), $data);
	})();
	$m['pane'] = (function(){
		var $cls_definition = new Object();
		var $method;
		$cls_definition['__module__'] = 'initiative';
		$method = $pyjs__bind_method2('__init__', function() {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
			}

			$m['SimplePanel']['__init__'](self);
			self['tbl'] = $m['FlexTable']();
			self['tbl']['setBorderWidth'](0);
			self['tbl']['setHTML'](0, 0, '<b>+</b>');
			self['tbl']['setHTML'](0, 1, '<b>Name</b>');
			self['tbl']['setHTML'](0, 2, '<b>Dmg</b>');
			self['characters'] = $p['list']([]);
			self['add']($p['getattr'](self, 'tbl'));
			self['initiative'] = 0;
			self['pressed'] = false;
			return null;
		}
	, 1, [null,null,['self']]);
		$cls_definition['__init__'] = $method;
		$method = $pyjs__bind_method2('add_character', function(character) {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
				character = arguments[1];
			}
			var index,$lambda1,$add10,$add7,$add8,$add9;
			self['characters'] = $p['__op_add']($add7=$p['getattr'](self, 'characters'),$add8=$p['list']([character]));
			var 			$lambda1 = function(c) {

				return (typeof ($usub1=$p['getattr'](c, 'initiative'))=='number'?
					-$usub1:
					$p['op_usub']($usub1));
			};
			$lambda1['__name__'] = '$lambda1';

			$lambda1['__bind_type__'] = 0;
			$lambda1['__args__'] = [null,null,['c']];
			$pyjs_kwargs_call(self['characters'], 'sort', null, null, [{'key':$lambda1}]);
			index = $p['__op_add']($add9=self['characters']['index'](character),$add10=1);
			self['tbl']['insertRow'](index);
			self['tbl']['setHTML'](index, 1, $p['getattr'](character, '$$name'));
			self['tbl']['setHTML'](index, 2, $p['str']($p['getattr'](character, 'hits')));
			self['tbl']['setWidget'](index, 3, $m['hitbutton'](self, character));
			self['tbl']['setWidget'](index, 4, $m['killbutton'](self, character));
			return null;
		}
	, 1, [null,null,['self'],['character']]);
		$cls_definition['add_character'] = $method;
		$method = $pyjs__bind_method2('advance', function() {
			if (this['__is_instance__'] === true) {
				var self = this;
			} else {
				var self = arguments[0];
			}
			var $add14,$add15,$add16,$add17,$add11,$add12,$add13,$add18;
			if ($p['bool']($p['getattr'](self, 'pressed'))) {
				self['tbl']['setHTML']($p['__op_add']($add11=$p['getattr'](self, 'initiative'),$add12=1), 0, '');
				self['initiative'] = $p['__op_add']($add13=$p['getattr'](self, 'initiative'),$add14=1);
				if ($p['bool'](((($p['cmp']($p['getattr'](self, 'initiative'), $p['len']($p['getattr'](self, 'characters'))))|1) == 1))) {
					self['initiative'] = 0;
				}
				self['tbl']['setHTML']($p['__op_add']($add15=$p['getattr'](self, 'initiative'),$add16=1), 0, '>');
			}
			else {
				self['pressed'] = true;
				self['tbl']['setHTML']($p['__op_add']($add17=$p['getattr'](self, 'initiative'),$add18=1), 0, '>');
			}
			return null;
		}
	, 1, [null,null,['self']]);
		$cls_definition['advance'] = $method;
		var $bases = new Array($m['SimplePanel']);
		var $data = $p['dict']();
		for (var $item in $cls_definition) { $data['__setitem__']($item, $cls_definition[$item]); }
		return $p['_create_class']('pane', $p['tuple']($bases), $data);
	})();
	$m['tbl'] = $m['pane']();
	$m['control'] = $m['HorizontalPanel']();
	$m['add_character'] = function(sender) {
		var $and4,$and3,initiative,name;
		name = $m['Window']['prompt']('Enter a name for the character.');
		initiative = $p['float_int']($m['Window']['prompt']($p['sprintf']('Initiative for %s:', name)));
		if ($p['bool'](($p['bool']($and3=!$p['op_eq'](name, null))?!$p['op_eq'](name, ''):$and3))) {
			$m['tbl']['add_character']($m['character'](name, initiative));
		}
		return null;
	};
	$m['add_character']['__name__'] = 'add_character';

	$m['add_character']['__bind_type__'] = 0;
	$m['add_character']['__args__'] = [null,null,['sender']];
	$m['end_turn'] = function(sender) {

		if ($p['bool'](($p['cmp']($p['len']($p['getattr']($m['tbl'], 'characters')), 0) == 1))) {
			$m['tbl']['advance']();
		}
		return null;
	};
	$m['end_turn']['__name__'] = 'end_turn';

	$m['end_turn']['__bind_type__'] = 0;
	$m['end_turn']['__args__'] = [null,null,['sender']];
	if ($p['bool']($p['op_eq']((typeof __name__ == "undefined"?$m['__name__']:__name__), '__main__'))) {
		$m['plus'] = $pyjs_kwargs_call(null, $m['Button'], null, null, [{'StyleName':'button'}, 'Add Character', $m['add_character']]);
		$m['turn'] = $pyjs_kwargs_call(null, $m['Button'], null, null, [{'StyleName':'button'}, 'End Turn', $m['end_turn']]);
		$m['control']['add']($m['plus']);
		$m['control']['add']($m['turn']);
		$m['RootPanel']()['add']($m['control']);
		$m['RootPanel']()['add']($m['tbl']);
	}
	return this;
}; /* end initiative */


/* end module: initiative */


/*
PYJS_DEPS: ['pyjamas.ui.RootPanel.RootPanel', 'pyjamas', 'pyjamas.ui', 'pyjamas.ui.RootPanel', 'pyjamas.ui.HorizontalPanel.HorizontalPanel', 'pyjamas.ui.HorizontalPanel', 'pyjamas.ui.VerticalPanel.VerticalPanel', 'pyjamas.ui.VerticalPanel', 'pyjamas.ui.Button.Button', 'pyjamas.ui.Button', 'pyjamas.ui.HTML.HTML', 'pyjamas.ui.HTML', 'pyjamas.Window', 'pyjamas.ui.FlexTable.FlexTable', 'pyjamas.ui.FlexTable', 'pyjamas.ui.SimplePanel.SimplePanel', 'pyjamas.ui.SimplePanel', 'pygwt']
*/
